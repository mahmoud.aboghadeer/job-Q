package com.onecard.job.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequestDTO {
	

	private String requestRefId;
	private String paymentMethod;

	public String getRequestRefId() {
		return requestRefId;
	}

	public void setRequestRefId(String requestRefId) {
		this.requestRefId = requestRefId;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	
}
