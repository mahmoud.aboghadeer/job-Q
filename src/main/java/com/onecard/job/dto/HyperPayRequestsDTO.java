package com.onecard.job.dto;

import java.io.Serializable;
import java.util.List;


public class HyperPayRequestsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<PaymentRequestDTO> requestRefList;

	public List<PaymentRequestDTO> getRequestRefList() {
		return requestRefList;
	}

	public void setRequestRefList(List<PaymentRequestDTO> requestRefList) {
		this.requestRefList = requestRefList;
	}

	

	
	
}
