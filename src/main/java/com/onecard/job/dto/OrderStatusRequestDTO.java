package com.onecard.job.dto;

import java.util.HashMap;
import java.util.Map;

public class OrderStatusRequestDTO {
	
	Map<String, String> checkStatusMap = new HashMap<>();

	public Map<String, String> getCheckStatusMap() {
		return checkStatusMap;
	}

	public void setCheckStatusMap(Map<String, String> checkStatusMap) {
		this.checkStatusMap = checkStatusMap;
	}
	
}