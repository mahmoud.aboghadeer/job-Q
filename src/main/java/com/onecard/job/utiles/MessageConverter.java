package com.onecard.job.utiles;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component
public class MessageConverter {

	protected final Log logger = LogFactory.getLog(getClass());
	private ObjectMapper objectMapper;
	private JsonNodeFactory jsonFactory = JsonNodeFactory.instance;


	public MessageConverter() {
		objectMapper = new ObjectMapper();
	}

	public <T> String convertObjectToJson(T obj) {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Can't convert object to json ", e);
		}

	}

	public <T> String convertListOfObjectToJson(List<T> listObj) {
		try {
			return objectMapper.writeValueAsString(listObj);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Can't convert object list to json ", e);
		}
	}

	public String buildCustomObjectError(String errorCode) {
		ObjectNode node = jsonFactory.objectNode();
		node.put("error", errorCode);
		return node.toString();
	}

	public <T> T convertJsonToObject(String jsonData, Class<T> clazz) {
		try {
			return objectMapper.readValue(jsonData, clazz);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Can't convert json to object ", e);
		}
	}
	
	public <T> List<T>  convertJsonToObjects(String jsonData, Class<T> clazz){
		try {
			List<T> resultList=objectMapper.readValue(jsonData, new TypeReference<List<T>>(){});
			return resultList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Can't convert json to object List ", e);
		}
		
	}
	
}
