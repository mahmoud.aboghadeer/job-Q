package com.onecard.job.utiles;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.onecard.job.dto.HyperPayRequestsDTO;
import com.onecard.job.dto.PaymentRequestDTO;

@PropertySource("classpath:api-resources.properties")
@Component
public class ConnectionUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionUtils.class);
	private static final String USER_NAME = "Bitaqaty";
	private static final String CONTENT_TYPE = "application/json";

	@Value("${bitaqatyWrapperUrl}" + "checkPaymentResponsByAdmin")
	private String bitaqatyWrapper;

	@Autowired
	MessageConverter messageConverter;

	public Object httpGetRequest(String uri) {

		HttpEntity<String> appendedHeader = appendHeader();
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, appendedHeader, String.class);
		return result.getBody();

	}

	public Object httpPostRequest(HttpEntity<String> requestEntity, String uri) {
		HttpEntity<String> appendedHeader = appendHeader(requestEntity);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		ResponseEntity<String> result = restTemplate.postForEntity(uri, appendedHeader, String.class);
		return result.getBody();
	}

	private HttpEntity<String> appendHeader(HttpEntity<String> requestEntity) {

		HttpHeaders headers = new HttpHeaders();
		handleHeaderNameValue(headers);
		return new HttpEntity<>(requestEntity.getBody(), headers);
	}

	private HttpEntity<String> appendHeader() {
		HttpHeaders headers = new HttpHeaders();
		handleHeaderNameValue(headers);
		return new HttpEntity<>(headers);
	}

	private void handleHeaderNameValue(HttpHeaders headers) {
		headers.set("Content-Type", CONTENT_TYPE);
		headers.set("Accept", CONTENT_TYPE);
		headers.set("userName", USER_NAME);
		headers.set("app-name", USER_NAME);
	}

	public Map<String, String> checkHyperPayRequests(HyperPayRequestsDTO hperPayRequests)
			throws NoSuchAlgorithmException, KeyManagementException {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		Map<String, String> checkStatusMap = new HashMap<String, String>();

		URL url;
		String res = null;
		try {
			for (PaymentRequestDTO paymentRequestDTO : hperPayRequests.getRequestRefList()) {
				url = new URL(bitaqatyWrapper);

				HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
				conn.addRequestProperty("userName", USER_NAME);
				conn.setRequestProperty("Accept", CONTENT_TYPE);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/xml; charset=utf-8");
				conn.setDoOutput(true);

				DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
				PaymentRequestDTO paymentRequestModel = new PaymentRequestDTO();
				paymentRequestModel.setRequestRefId(paymentRequestDTO.getRequestRefId());
				paymentRequestModel.setPaymentMethod(paymentRequestDTO.getPaymentMethod());
				String data = messageConverter.convertObjectToJson(paymentRequestModel);
				wr.writeBytes(data);

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				res = br.readLine();
				checkStatusMap.put(paymentRequestDTO.getRequestRefId(), res);

			}

		} catch (Exception e) {
			LOGGER.info("wrapperPostRequestException", e);
		}
		return checkStatusMap;
	}
}
