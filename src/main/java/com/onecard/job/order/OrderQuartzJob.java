package com.onecard.job.order;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;

import com.onecard.job.dto.HyperPayRequestsDTO;
import com.onecard.job.dto.OrderStatusRequestDTO;
import com.onecard.job.utiles.ConnectionUtils;
import com.onecard.job.utiles.MessageConverter;

public class OrderQuartzJob implements Job {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderQuartzJob.class);

	@Value("${bitaqatyApiUrl}" + "checkRequestStatusUrl")
	private String checkRequestStatusUrl;

	@Value("${bitaqatyApiUrl}" + "requestsNotCompletedUrl")
	private String requestsNotCompletedUrl;

	@Autowired
	ConnectionUtils connectionUtils;

	@Autowired
	MessageConverter messageConverter;

	@Override
	public void execute(JobExecutionContext jobExecutionContext) {
		HyperPayRequestsDTO hperPayRequests = messageConverter.convertJsonToObject(
				(String) connectionUtils.httpGetRequest(requestsNotCompletedUrl), HyperPayRequestsDTO.class);

		if (hperPayRequests != null && hperPayRequests.getRequestRefList() != null
				&& !hperPayRequests.getRequestRefList().isEmpty()) {
			Map<String, String> checkStatusMap = null;
			try {
				checkStatusMap = connectionUtils.checkHyperPayRequests(hperPayRequests);
			} catch (Exception e) {
				LOGGER.info("CheckHyperPayRequestsException", e);
			}
			
			if (checkStatusMap != null) {
				OrderStatusRequestDTO orderStatusRequestDTO = new OrderStatusRequestDTO();
				orderStatusRequestDTO.setCheckStatusMap(checkStatusMap);
				String requestBody = messageConverter.convertObjectToJson(orderStatusRequestDTO);
				HttpEntity<String> entity = new HttpEntity<>(requestBody);
				connectionUtils.httpPostRequest(entity, checkRequestStatusUrl);
			}
			
		}

	}

	

}